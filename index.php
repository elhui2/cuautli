<?php

/**
 * Cuautli, (Eagle in nahuatl)
 * @author Daniel Huidobro <contacto@elhui2.info> http://elhui2.info
 * @license GNU/GPL Version 3 Or later.
 * @version 0.1 Initialize Git Repo
 * @copyright (c) 2015, Daniel Huidobro
 * @tutorial http://www.admiror-design-studio.com/joomla-extensions
 * @uses http://minimuslider.musings.it/
 * Fork to Gallery View
 */

// Joomla security code
defined('_JEXEC') or die();

//Load stylesheet and javascript
$AG->loadCSS($AG->currTemplateRoot . 'css' . DIRECTORY_SEPARATOR . 'cuautli.css');
$AG->loadJS($AG->currTemplateRoot . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'cuautli.js');

$html = '<div id="cuautli-container">';
$x = 1;

foreach ($AG->images as $imageKey => $imageName) {
    if ($x == 1) {
        $html .= '<div class="ct-controls">'
                . '<button id="ct-list" active="0">Lista</botton><button id="ct-fullscreen">Full Screen</button><button id="ct-carousel" style="display:none">Carrusel</button>'
                . '</div>'
                . '<img src="' . $AG->sitePath . $AG->params['rootFolder'] . $AG->imagesFolderName . '/' . $imageName . '" data-src="' . $AG->sitePath . $AG->params['rootFolder'] . $AG->imagesFolderName . '/' . $imageName . '" alt="' . $AG->writeDescription($imageName) . '" title="' . $AG->writeDescription($imageName) . '" />'
                . '<p>' . $AG->writeDescription($imageName) . '</p>'
                . '<input class="cuautli-item" type="hidden" src="' . $AG->sitePath . $AG->params['rootFolder'] . $AG->imagesFolderName . '/' . $imageName . '" data-src="' . $AG->sitePath . $AG->params['rootFolder'] . $AG->imagesFolderName . '/' . $imageName . '" alt="' . $AG->writeDescription($imageName) . '" title="' . $AG->writeDescription($imageName) . '" />';
    }else{
        $html .= '<input class="cuautli-item" type="hidden" src="' . $AG->sitePath . $AG->params['rootFolder'] . $AG->imagesFolderName . '/' . $imageName . '" data-src="' . $AG->sitePath . $AG->params['rootFolder'] . $AG->imagesFolderName . '/' . $imageName . '" alt="' . $AG->writeDescription($imageName) . '" title="' . $AG->writeDescription($imageName) . '" />';
    }
    $x++;
}

$html .= '<div class="cuautli-listview"></div></div>';