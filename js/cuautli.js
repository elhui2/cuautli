jQuery(document).ready(function ($) {
    $('body').append('<div id="overlay-cuautli" style="display:none"><div id="cuautli-slideshow"></div><div id="loop" class="center"></div><div id="bike-wrapper" class="center"><div id="bike" class="centerBike"></div></div><button class="cuautli-close">cerrar</button><button class="cuautli-prev"><span><<span></button><button class="cuautli-next"><span>><span></button><div class="cuautli-description"></div></div>');
    var cuautliData = new Array();
    $('#cuautli-container').each(function () {
        $(this).find('input').each(function () {
            var dataItem = new Array($(this).attr('src'), $(this).attr('title'));
            cuautliData.push(dataItem);
        });
    });
    $('#ct-fullscreen').click(function () {
        $('#overlay-cuautli').fadeIn('fast');

//        var cuautliData = new Array();
//        $('#cuautli-container').each(function () {
//            $(this).find('input').each(function () {
//                var dataItem = new Array($(this).attr('src'), $(this).attr('title'));
//                cuautliData.push(dataItem);
//            });
//        });

        setTimeout(function () {
            var indice = 0;
            $('.cuautli-close').click(function () {
                $('#overlay-cuautli').fadeOut('fast');
            });

            $('#cuautli-slideshow').html('<img src="' + cuautliData[indice][0] + '" alt="' + cuautliData[indice][1] + '" ><p>' + cuautliData[indice][1] + '<p>');

            $('.cuautli-next').click(function () {
                console.log('Next: Indice de la galeria=> ' + indice + ' length=>' + (cuautliData.length - 1));
                if (indice === (cuautliData.length - 1)) {
                    return;
                }
                indice++;
                $('#cuautli-slideshow').fadeOut('fast', function () {
                    $('#cuautli-slideshow').html('<img src="' + cuautliData[indice][0] + '" alt="' + cuautliData[indice][1] + '" ><p>' + cuautliData[indice][1] + '<p>');
                    $(this).fadeIn('fast');
                });
            });
            $('.cuautli-prev').click(function () {
                console.log('Prev: Indice de la galeria=> ' + indice);
                if (indice === 0) {
                    return;
                }
                indice--;
                $('#cuautli-slideshow').fadeOut('fast', function () {
                    $('#cuautli-slideshow').html('<img src="' + cuautliData[indice][0] + '" alt="' + cuautliData[indice][1] + '" ><p>' + cuautliData[indice][1] + '<p>');
                    $(this).fadeIn('fast');
                });
            });
        }, 1500);
    });
    $('#ct-list').click(function () {
        var active = $(this).attr('active');
        if (active == 0) {
            
            for (var i = 0; i < cuautliData.length; i++) {
                if (i > 0) {
                    $('#cuautli-container .cuautli-listview').append('<img src="' + cuautliData[i][0] + '" alt="' + cuautliData[i][1] + '" ><p>' + cuautliData[i][1] + '<p>');
                }
            }
            $(this).attr('active',1);

        } else {
            $('#cuautli-container .cuautli-listview').empty();
            $(this).attr('active',0);
        }
    });

});